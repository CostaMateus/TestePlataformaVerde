<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResiduesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residues', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');          // Nome Comum do Resíduo
            $table->string('type');          // Tipo de Resíduo
            $table->string('category');      // Categoria
            $table->string('treatment');     // Tecnologia de Tratamento
            $table->string('class');         // Classe
            $table->string('unity');         // Unidade de Medida
            $table->double('weight', 12, 3); // Peso

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residues');
    }
}
