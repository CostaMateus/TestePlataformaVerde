<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/residue',                   'ResidueController@index'  )->name('residue.index');

Route::get('/residue/{residue}/edit',    'ResidueController@edit'   )->name('residue.edit');
Route::post('/residue/{residue}/update', 'ResidueController@update' )->name('residue.update');

Route::get('/residue/{residue}/delete',  'ResidueController@destroy')->name('residue.destroy');

Route::get('/import',                    'ResidueController@import' )->name('residue.import');
