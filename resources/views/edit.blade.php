<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>PlataformaVerde | Editar</title>

		<!-- Font Awesome Free CDN -->
		<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body { background-color: #fff; color: #636b6f; font-family: 'Nunito', sans-serif; font-weight: 200; height: 100vh; margin: 0; }
            .full-height { height: 100vh; }
            .position-ref { position: relative; }
            .top-right { position: absolute; right: 10px; top: 18px; }
            .title { font-size: 84px; }
            .links > a { color: #636b6f; padding: 0 25px; font-size: 13px; font-weight: 600; letter-spacing: .1rem; text-decoration: none; text-transform: uppercase; }
            .links > a:hover { color: #808000; }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="title mb-4 text-center">
                PlataformaVerde
            </div>

            <div class="links text-center">
                <p>Planilha de resíduos</p>
            </div>

            <section class="col-6 mx-auto">
                <form action="/residue/{{ $residue->id }}/update" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name" >Nome Comum do Resíduo</label>
                        <input type="text" class="form-control" id="name" name="name"  value="{{ $residue->name }}">
                    </div>
                    <div class="form-group">
                        <label for="type" >Tipo de Resíduo</label>
                        <input type="text" class="form-control" id="type" name="type" value="{{ $residue->type }}">
                    </div>
                    <div class="form-group">
                        <label for="category" >Categoria</label>
                        <input type="text" class="form-control" id="category" name="category" value="{{ $residue->category }}">
                    </div>
                    <div class="form-group">
                        <label for="treatment" >Tecnologia de Tratamento</label>
                        <input type="text" class="form-control" id="treatment" name="treatment" value="{{ $residue->treatment }}">
                    </div>
                    <div class="form-group">
                        <label for="class" >Classe</label>
                        <input type="text" class="form-control" id="class" name="class" value="{{ $residue->class }}">
                    </div>
                    <div class="form-group">
                        <label for="unity" >Unidade de Medida</label>
                        <input type="text" class="form-control" id="unity" name="unity" value="{{ $residue->unity }}">
                    </div>
                    <div class="form-group">
                        <label for="weight" >Peso</label>
                        <input type="number" class="form-control" id="weight" name="weight" value="{{ $residue->weight }}" step="0.001" min="0.001" >
                    </div>

                    <button type="submit" class="btn btn-primary">Salvar</button>
                </form>
            </section>
        </div>
    </body>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</html>
