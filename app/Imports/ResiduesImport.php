<?php

namespace App\Imports;

use App\Residue;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('slug');

class ResiduesImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        $timestamp = date('Y-m-d');

        $data = [];

        foreach ($rows as $row)
        {
            if ($row['nome_comum_do_residuo'] != null)
            {
                $data[] = [
                    'name'       => $row['nome_comum_do_residuo'],
                    'type'       => $row['tipo_de_residuo'],
                    'category'   => $row['categoria'],
                    'treatment'  => $row['tecnologia_de_tratamento'],
                    'class'      => $row['classe'],
                    'unity'      => $row['unidade_de_medida'],
                    'weight'     => $row['peso'],
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp,
                    'deleted_at' => NULL
                ];
            }
        }

        Residue::insert($data);
    }
}
