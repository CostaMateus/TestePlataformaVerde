<?php

namespace App\Http\Controllers;

use App\Imports\ResiduesImport;
use App\Residue;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ResidueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $residues = Residue::all();

        return view('residues', ['residues' => $residues]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Residue  $residue
     * @return \Illuminate\Http\Response
     */
    public function show(Residue $residue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Residue  $residue
     * @return \Illuminate\Http\Response
     */
    public function edit(Residue $residue)
    {
        return view('edit', ['residue' => $residue]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Residue  $residue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Residue $residue)
    {
        $validatedData = $request->validate([
            'name'      => 'required|string',
            'type'      => 'required|string',
            'category'  => 'required|string',
            'treatment' => 'required|string',
            'class'     => 'required|string',
            'unity'     => 'required|string',
            'weight'    => 'required|numeric',
        ]);


        $residue->fill($validatedData);

        $residue->save();

        return redirect('/residue');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Residue  $residue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Residue $residue)
    {
        $residue->delete();

        return redirect('/residue');
    }

    /**
     *
     *
     * @param  \App\Residue  $residue
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $residue)
    {
        $residue = residue::withTrashed()->find($residue);

        $residue->forceDelete();

        return $residue;
    }

    /**
     *
     *
     * @param  \App\Residue  $residue
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $residue)
    {
        $residue = residue::withTrashed()->find($residue);

        $residue->restore();

        return $residue;
    }

    /**
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        return view('excel');
    }

    /**
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function importExcel(Request $request)
    {
        $validatedData = $request->validate([
            'file' => 'required|max:5000|mimes:xlsx,xls,csv'
        ]);

        // $date = date('Ymd_His');
        // $file = $request->file('file');
        // $fileName = $date . '-' . $file->getClientOriginalName();
        // $savePath = public_path('/upload/');
        // $file->move($savePath, $fileName);
        // $path = $savePath.$fileName;

        if ($request->file('file')) {
            // Excel::import(new ResiduesImport(), $path);
            Excel::import(new ResiduesImport(), $validatedData['file']->path());

            // return back()->with('success', 'Arquivo importado com sucesso.');
        }

        return redirect('/import')->with('success', 'Arquivo importado com sucesso.');
    }

}
